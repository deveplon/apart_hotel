module.exports = {
  extends: 'airbnb',
  rules: {
    'import/no-dynamic-require': 0,
    'global-require': 0,
    'valid-typeof': 0,
  },
};
