/**
 * @todo
 *  - Get user to authenticate
 *  - make create user (register)
 *  - make update user
 *  - make delete user
 */

/**
 * @description Login user
 *
 * @param object request
 * @param object respose
 */
exports.login = async (req, res) => res.json({ code: 1 });

/**
 * @description Logout user
 *
 * @param object request
 * @param object respose
 */
exports.logout = async (req, res) => res.json({ code: 1 });
