/**
 * User post (Create User)
 *
 * @param object request
 * @param object respose
 */

// Core Libraries
const path = require('path');

// User libraries/Schemas
const User = require('./schema.js');

const { requiredParams, compareParams } = require(path.join(__dirname, '../..', 'libs/validator'));
const { encryptPass, generateRandom } = require(path.join(__dirname, '../..', 'libs/utils'));

// Process
const required = {
  name: 'string',
  username: 'string',
  email: 'string',
  phone: 'number',
  password: 'string',
  confirmPassword: 'string',
};

async function post(params, res) {
  const { salt, hash } = await encryptPass(params.password);

  const {
    name, username, email, phone,
  } = params;

  const user = new User({
    name,
    username,
    email,
    phone,
    hash,
    salt,
    recoverPass: generateRandom(),
  });

  try {
    await user.save();

    return res.json({
      code: 1,
      message: 'user sucessfully saved',
    });
  } catch (err) {
    throw new Error();
  }
}

module.exports = (req, res) => {
  const { body } = req;

  if (!requiredParams(body, required) && !compareParams(body.password, body.confirmPassword)) {
    throw new Error('invalid-request');
  }

  post(body, res);
};
