const express = require('express');

const user = require('./user.js');

const router = express.Router();

router.get('/', user.get);
router.post('/', user.post);
router.put('/', user.put);
router.patch('/', user.patch);
router.delete('/', user.delete);

module.exports = router;
