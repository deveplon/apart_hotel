const mongoose = require('mongoose');

const { Schema } = mongoose;

const UserSchema = new Schema(
  {
    name: String,
    username: {
      type: String,
      unique: true,
      trim: true,
      lowercase: true,
      required: true,
      index: true,
    },
    email: {
      type: String,
      unique: true,
      trim: true,
      lowercase: true,
      required: true,
      index: true,
    },
    phone: Number,
    hash: {
      type: String,
      required: true,
    },
    salt: {
      type: String,
      required: true,
    },
    recoverPass: String,
    status: {
      type: Number,
      default: 1,
    },
  },
  { timestamps: true },
);

const User = mongoose.model('User', UserSchema);

module.exports = User;
