/**
 * @todo
 *  - Get user to authenticate
 *  - make create user (register)
 *  - make update user
 *  - make delete user
 */

// const User = function User() {};

/**
 * @description Api Get (Get User)
 *
 * @param object request
 * @param object respose
 */

exports.get = async (req, res) => res.json({ code: 1 });

/**
 * @description Api post (Create User)
 */
exports.post = require('./post.js');

/**
 * @description Api put (Update User)
 *
 * @param object request
 * @param object respose
 */
exports.put = async (req, res) => res.json({ code: 1 });

/**
 * @description Api patch (Update User property)
 *
 * @param object request
 * @param object respose
 */
exports.patch = async (req, res) => res.json({ code: 1 });

/**
 * @description Api delete (Delete User)
 *
 * @param object request
 * @param object response
 */
exports.delete = async (req, res) => res.json({ code: 1 });
