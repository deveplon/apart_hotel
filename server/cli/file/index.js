#!/usr/bin/env node

const readline = require('readline');
const fs = require('fs');
const path = require('path');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const options = ['schema'];

const optionsMsg = `//*** FILE SYSTEM CREATOR ***//

Options:
  
  - schema

`;

function fileName() {
  return new Promise((resolve, reject) => {
    rl.question('What type of file do you want to create? ', (answer) => {
      if (answer.trim() === '' || !options.includes(answer.trim())) {
        reject(
          new Error(`\nType of file "${answer}" not found. Thank for use the file system creator`),
        );
      }

      resolve(answer);
    });
  });
}

function typeName() {
  return new Promise((resolve, reject) => {
    rl.question('What type of service do you want to create? ', (answer) => {
      if (answer.trim() === '') {
        reject(
          new Error(
            `\nInvalid type of service name "${answer}". Thank for use the file system creator`,
          ),
        );
      }

      try {
        const stats = fs.statSync(path.join(__dirname, '../..', answer));
        if (!stats.isDirectory()) {
          reject(
            new Error(
              `\nType of service "${answer}" not found. Thank for use the file system creator`,
            ),
          );
        }
      } catch (err) {
        reject(
          new Error(
            `\nType of service "${answer}" not found. Thank for use the file system creator`,
          ),
        );
      }

      resolve(answer);
    });
  });
}

function createFile(file, type) {
  return new Promise((resolve, reject) => {
    rl.question('What service do you want to create? ', (answer) => {
      if (answer.trim() === '') {
        reject(
          new Error(`\nInvalid service name "${answer}". Thank for use the file system creator`),
        );
      }

      try {
        fs.statSync(path.join(__dirname, '../..', type, answer));
      } catch (err) {
        fs.mkdirSync(path.join(__dirname, '../..', type, answer));
      }

      const data = require(`./${file}.js`)(answer);

      fs.writeFileSync(path.join(__dirname, '../..', type, answer, `${file}.js`), data, {
        encoding: 'utf8',
        flag: 'w+',
      });

      resolve('File is created');
    });
  });
}

async function init() {
  try {
    await rl.write(optionsMsg);

    const file = await fileName();

    const type = await typeName();

    const response = await createFile(file, type);

    rl.write(response);
    rl.close();
  } catch (error) {
    rl.write(error.message);
    rl.close();
  }
}

init();
