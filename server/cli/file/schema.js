module.exports = (name) => {
  const entity = name[0].toUpperCase() + name.slice(1);

  return `const mongoose = require('mongoose');

const { Schema } = mongoose;

const ${entity}Schema = new Schema({});

const ${entity} = mongoose.model('${entity}', ${entity}Schema);

module.exports = ${entity};
`;
};
