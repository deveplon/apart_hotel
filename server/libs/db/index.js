/**
 * @name Database
 * @description MongoDB database
 */

const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/ApartHotel', { useNewUrlParser: true });

mongoose.set('useCreateIndex', true);

const db = mongoose.connection;

/* eslint-disable no-console */
db.on('error', () => {
  console.error('connection error:');
});

db.once('open', () => {
  console.log('Database connected');
});
