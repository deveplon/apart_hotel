/**
 * @name AppError
 * @description Responses module
 */

const types = {
  'invalid-request': {
    statusCode: 400,
    message: 'Invalid request',
  },
  'not-found': {
    statusCode: 404,
    message: 'Cannot find',
  },
};

const AppError = function AppError(type) {
  this.name = 'AppError';
  this.type = type;
};

AppError.prototype.getLast = function getLast() {
  return types[this.type];
};

module.exports = AppError;
