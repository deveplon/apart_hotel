const AppError = require('./error.js');

/* eslint-disable no-unused-vars */
module.exports = (err, req, res, next) => {
  const type = err.message;

  const appError = new AppError(type);
  const lastError = appError.getLast();

  try {
    return res.status(lastError.statusCode).json({
      code: 1,
      message: lastError.message,
      method: req.method,
      url: req.originalUrl,
      params: req.method === 'GET' ? req.query : req.body,
    });
  } catch (error) {
    return res.status(500).json({ code: 1, message: 'We are experimenting some problems' });
  }
};
