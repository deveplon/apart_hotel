const fs = require('fs');

const Log = function Log(type, msg) {
  this.type = type;
  this.msg = msg;
};

Log.prototype.error = function error() {};

module.exports = new Log();
