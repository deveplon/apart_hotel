/**
 * @name Utils
 * @description Utilities
 */

const { encryptPass, generateRandom } = require('./passwords');

exports.encryptPass = encryptPass;
exports.generateRandom = generateRandom;
