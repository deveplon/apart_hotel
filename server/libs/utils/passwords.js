/**
 * @name Password
 * @description password util methods
 */

const crypto = require('crypto');

/**
 * Encrypt password with pbkdf2Sync crypto method
 *
 * @param {String} password
 * @returns {Object} Salt and Hash generated
 */
exports.encryptPass = (password) => {
  try {
    const salt = crypto.randomBytes(32).toString('hex');
    const hash = crypto.pbkdf2Sync(password, salt, 1000, 64, 'sha512').toString('hex');

    return { salt, hash };
  } catch (err) {
    throw new Error();
  }
};

/**
 * Generate Random bytes to string
 *
 * @returns {String} Generated random string
 */
exports.generateRandom = () => crypto.randomBytes(64).toString('hex');
