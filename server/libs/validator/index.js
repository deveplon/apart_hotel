/**
 * @name Validator
 * @description Validations
 */

const { requiredParams, compareParams } = require('./params.js');

exports.requiredParams = requiredParams;
exports.compareParams = compareParams;
