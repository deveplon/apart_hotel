/**
 * Validate the required params
 *
 * @param {Object} requiredParams
 * @param {Object} params
 */
exports.requiredParams = (params, requiredParams) => {
  try {
    if (!params || typeof params !== 'object') return false;

    const validation = Object.keys(params).filter((key) => {
      const param = params[key];
      const type = requiredParams[key] || null;
      return type && typeof param === type;
    });

    return validation.length === Object.keys(requiredParams).length;
  } catch (err) {
    return false;
  }
};

/**
 * Compare tow stings or numbers
 *
 * @param {String|Number} param
 * @param {String|Number} comparisson
 * @returns
 */
exports.compareParams = (param, comparisson) => {
  if (!param || !comparisson) return false;

  if (typeof param !== 'string' || typeof param !== 'number') return false;

  return param.trim() === comparisson.trim();
};
