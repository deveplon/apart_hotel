const path = require('path');

module.exports = (app) => {
  app.use('/api/user', require(path.join(__dirname, 'api', 'user')));
  app.use('/api/auth', require(path.join(__dirname, 'api', 'auth')));

  app.use(require(path.join(__dirname, 'libs', 'error')));
};
