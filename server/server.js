/**
 * @name Server
 * @description Api express server to apart Hotel
 */

const express = require('express');
const path = require('path');

const port = process.env.PORT || 4500;

const app = express();
const router = require('./router.js');

require(path.join(__dirname, 'libs', 'db'));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

router(app);

app.listen(port, () => {
  /* eslint-disable no-console */
  console.log(`Listening on port ${port}`);
});
